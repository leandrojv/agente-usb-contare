package comportsendreceive;

import jssc.SerialPortList;

/**
 * Created by ag3 on 06/09/17.
 */
public class GetPorts {

    public static void main(String[] args) {
        //Method getPortNames() returns an array of strings. Elements of the array is already sorted.
        String[] portNames = SerialPortList.getPortNames();

        System.out.println("Portas encontradas : " + portNames.length);


        for (int i = 0; i < portNames.length; i++) {
            System.out.println(portNames[i]);
        }
    }

}
