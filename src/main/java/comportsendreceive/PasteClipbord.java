package comportsendreceive;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.event.KeyEvent;
import java.io.IOException;

public final class PasteClipbord implements ClipboardOwner{

    private static PasteClipbord pasteClipbord =  new PasteClipbord();
    private static Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();

    public static void paste(String msg)  {


        pasteClipbord.setClipboardContents(msg);

        Robot robot = null;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        if (robot == null) {
            return;
        }

        int ctrlOrCmdKey = -1;
        if (System.getProperty("os.name").toLowerCase().contains("mac")) {
            ctrlOrCmdKey = KeyEvent.VK_META;
        } else {
            ctrlOrCmdKey = KeyEvent.VK_CONTROL;
        }

        try {

            robot.keyPress(ctrlOrCmdKey);
            Thread.sleep(10);
            robot.keyPress(KeyEvent.VK_V);
            Thread.sleep(10);
            robot.keyRelease(KeyEvent.VK_V);
            Thread.sleep(10);
            robot.keyRelease(ctrlOrCmdKey);
            Thread.sleep(10);
            robot.keyPress(KeyEvent.VK_ENTER);
            Thread.sleep(10);
            robot.keyRelease(KeyEvent.VK_ENTER);
            Thread.sleep(10);



        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {
        contents = clipboard.getContents(this);
        regainOwnership(contents);

    }

    private void regainOwnership(Transferable t) {
        clipboard.setContents(t, this);
    }

    private void setClipboardContents(String aString){
        StringSelection stringSelection = new StringSelection(aString);
        clipboard.setContents(stringSelection, this);
    }

    public String getClipboardContents() {
        String result = "";
        //odd: the Object param of getContents is not currently used
        Transferable contents = clipboard.getContents(null);
        boolean hasTransferableText =
                (contents != null) &&
                        contents.isDataFlavorSupported(DataFlavor.stringFlavor)
                ;
        if (hasTransferableText) {
            try {
                result = (String)contents.getTransferData(DataFlavor.stringFlavor);
            }
            catch (UnsupportedFlavorException | IOException ex){
                System.out.println(ex);
                ex.printStackTrace();
            }
        }
        return result;
    }
}
