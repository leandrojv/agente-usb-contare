package comportsendreceive;

import jssc.SerialPort;
import jssc.SerialPortException;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by ag3 on 06/09/17.
 */
public class ReadWrite {


    public static void main(String[] args) throws UnsupportedEncodingException {

        SerialPort serialPort = new SerialPort("COM4");
        try {

            serialPort.openPort();
            serialPort.setParams(115200, 8, 1, 0);

            serialPort.writeBytes(new BigInteger(Comandos.GET_SESSION, 16).toByteArray());
            byte[] buffer = serialPort.readBytes(10);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < buffer.length; i++) {

                byte b = buffer[i];
                sb.append(String.format("%02X ", b));

            }
            System.out.println(sb.toString());

            serialPort.closePort();
        } catch (SerialPortException ex) {
            System.out.println(ex);
        }
    }
}
