package comportsendreceive;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

import java.math.BigInteger;

public class Listener {

    static SerialPort serialPort;
    private static StringBuilder epc = new StringBuilder();
    private static Integer posicaoLeitura = 0;

    public static void main(String[] args) {
        serialPort = new SerialPort("COM4");
        try {

            serialPort.openPort();
            serialPort.setParams(115200, 8, 1, 0);

            serialPort.writeBytes(new BigInteger(Comandos.START_READ, 16).toByteArray());

//            int mask = SerialPort.MASK_RXCHAR;
//            serialPort.setEventsMask(mask);

            serialPort.addEventListener(new SerialPortReader());

        } catch (SerialPortException ex) {
            System.out.println(ex);
        }
    }

    static class SerialPortReader implements SerialPortEventListener {

        public void serialEvent(SerialPortEvent event) {
            //Object type SerialPortEvent carries information about which event occurred and a value.
            //For example, if the data came a method event.getEventValue() returns us the number of bytes in the input buffer.

            try {

                byte buffer[] = serialPort.readBytes();

                for (int i = 0; i < buffer.length; i++) {
                    byte b = buffer[i];
                    generateEPC(b);
                }

            } catch (SerialPortException ex) {
                System.out.println(ex);
            }

        }
    }

    public static void generateEPC(byte b) {

        if (posicaoLeitura == 0) {
            posicaoLeitura = (String.format("%02X", b).equals("BB")) ? 1 : 0;

        } else if (posicaoLeitura == 1) {
            posicaoLeitura = (String.format("%02X", b).equals("02")) ? 2 : 0;
        } else if (posicaoLeitura == 2) {
            posicaoLeitura = (String.format("%02X", b).equals("22")) ? 3 : 0;

        } else if (posicaoLeitura == 3) {
            posicaoLeitura = (String.format("%02X", b).equals("00")) ? 4 : 0;

        } else if (posicaoLeitura == 4) {
            posicaoLeitura = (String.format("%02X", b).equals("0E")) ? 5 : 0;

        } else if (posicaoLeitura == 5) {
            posicaoLeitura = (String.format("%02X", b).equals("30")) ? 6 : 0;

        } else if (posicaoLeitura == 6) {
            posicaoLeitura = (String.format("%02X", b).equals("00")) ? 7 : 0;

        } else if (posicaoLeitura == 7) {
            if (epc.length() == 24) {
                posicaoLeitura = 0;

                try {
                    PasteClipbord.paste(epc.toString());
                    Thread.sleep(10);
                    System.out.println("destravou");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                epc = new StringBuilder();
            } else {
                epc.append((String.format("%02X", b)));
            }
        }
    }

}
