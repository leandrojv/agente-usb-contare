/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package comportsendreceive;

import com.fazecast.jSerialComm.SerialPort;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author mauricio.cirelli
 *         <p>
 *         Driver de comunicação com o GPS.
 *         Responsável por receber os dados do GPS
 *         e gerar eventos de posicionamento.
 */
public class GPSDriver {

    private SerialPort comPort;
    private final String serialPort = "COM4";
    private Boolean isRunning;



    public GPSDriver() {

        SerialPort[] commPorts = SerialPort.getCommPorts();
        String[] portNames = new String[commPorts.length];
        for (int i = 0; i < commPorts.length; i++) {
            portNames[i] = commPorts[i].getSystemPortName();
            if (commPorts[i].getSystemPortName().equals(serialPort)) {
                Logger.getLogger(GPSDriver.class.getName()).log(Level.INFO, "Found Serial Port {0}.", commPorts[i].getSystemPortName());
                comPort = commPorts[i];
            }
        }

        if (comPort == null) {
            Logger.getLogger(GPSDriver.class.getName()).log(Level.SEVERE, "Serial Port {0} not found.", serialPort);
            StringBuilder builder = new StringBuilder();
            builder.append("Available ports are: ");
            for (String s : portNames) {
                builder.append(s);
                builder.append(" ");
            }
            Logger.getLogger(GPSDriver.class.getName()).log(Level.SEVERE, builder.toString());
            throw new RuntimeException("Serial Port " + serialPort + " not found.");
        }
    }


    public void open() {
        comPort.openPort();
        comPort.setComPortTimeouts(SerialPort.TIMEOUT_READ_SEMI_BLOCKING, 1000, 0);
    }

    public void close() {
        if (comPort.isOpen()) {
            comPort.closePort();
        }
    }

//    public void start() {
//        try {
//            Process startModemProcess = RuntimeUtil.execAndWait("restartGps");
//        } catch (Exception ex) {
//            Logger.getLogger(ModemDriver.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
//        }
//
//        isRunning = true;
//
//        Thread t = new Thread(new Runnable() {
//
//            @Override
//            public void run() {
//                char c;
//                InputStream in = comPort.getInputStream();
//
//                while (isRunning) {
//                    try {
//                        Util.tick(UcpProcess.GPS_DRIVER);
//
//                        StringBuilder builder = new StringBuilder();
//                        do {
//                            c = (char) in.read();
//                            builder.append(c);
//                        }
//                        while (c != '\n');
//
//                        String nmea = builder.toString();
//                        if (nmea.contains("GGA") || nmea.contains("RMC")) {
//                            Logger.getLogger(GPSDriver.class.getName()).log(Level.FINEST, nmea);
//                        }
//
//                        //TODO - temporario - remover
//                        if (nmea.contains("STATUS")) {
//                            LocalLogger.addGpsLog(nmea);
//                        }
//
//                        if (parser.parse(nmea, lastPosition)) {
//                            for (GPSEventListener listener : listeners) {
//                                listener.onNewPosition(lastPosition);
//                            }
//                            lastPosition = new GPSData();
//                        }
//
//                    } catch (IOException ex) {
//                        Logger.getLogger(GPSDriver.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
//                    }
//                }
//            }
//
//        });
//
//        t.start();
//    }

    public void stop() {
        isRunning = false;
    }

}
